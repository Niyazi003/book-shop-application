package org.example.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
//@Data
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class MarketEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Double amount;
    private Integer bookCount;
    @OneToMany(cascade = CascadeType.ALL)
    @JsonIgnore
    private List<BookEntity> books;
}
