package org.example.repo;

import org.example.domain.RatingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RatingRepo extends JpaRepository<RatingEntity,Long> {
    RatingEntity findByName(String name);
    @Query("SELECT SUM(r.count) FROM RatingEntity r")
    Integer findSumOfAllCounts();
}
