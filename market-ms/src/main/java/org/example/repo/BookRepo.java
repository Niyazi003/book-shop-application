package org.example.repo;

import org.example.domain.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BookRepo extends JpaRepository<BookEntity, Long> {
    @Override
    Optional<BookEntity> findById(Long id);

    Optional<BookEntity> findByName(String name);

    Optional<List<BookEntity>> findAllByName(String name);
}
