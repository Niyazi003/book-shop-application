package org.example.repo;

import org.example.domain.MarketEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MarketRepo extends JpaRepository<MarketEntity,Long> {
    @Override
    @EntityGraph(attributePaths = "books")
    Optional<MarketEntity> findById(Long id);
}
