package org.example.service;

import org.example.domain.BookEntity;
import org.example.dto.BookDto;
import org.example.dto.BookResponseDto;

import java.util.List;

public interface BookService {
    BookResponseDto createBook(Long id, BookDto bookDto);
    BookResponseDto findBook(Long id);
    List<BookResponseDto> findBookByName(String name);

}
