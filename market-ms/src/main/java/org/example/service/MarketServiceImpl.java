package org.example.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.example.domain.BookEntity;
import org.example.domain.MarketEntity;
import org.example.dto.MarketDto;
import org.example.dto.MarketResponseDto;
import org.example.exception.NotFoundException;
import org.example.repo.BookRepo;
import org.example.repo.MarketRepo;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {
    private final MarketRepo marketRepo;
    private final ModelMapper modelMapper;
    private final BookRepo bookRepo;
    @Override
    public MarketResponseDto createMarket(MarketDto marketDto) {
        MarketEntity marketEntity = MarketEntity
                .builder()
                .name(marketDto.getName())
                .amount(marketDto.getTotalAmount())
                .build();
        marketRepo.save(marketEntity);
        return MarketResponseDto
                .builder()
                .name(marketEntity.getName())
                .amount(marketEntity.getAmount())
                .build();
    }

    @Override
    public MarketResponseDto findMarket(Long id) {
        MarketEntity marketEntity = marketRepo.findById(id).orElseThrow(NotFoundException::new);
        return modelMapper.map(marketEntity,MarketResponseDto.class);
    }

    @Override
    public MarketResponseDto updateMarket(Long id,MarketDto marketDto) {
        MarketEntity marketEntity = marketRepo.findById(id).orElseThrow(NotFoundException::new);
        modelMapper.map(marketDto,marketEntity);
        marketRepo.save(marketEntity);
        return modelMapper.map(marketEntity,MarketResponseDto.class);
    }
@Override
    public void applyUpdates(MarketEntity marketEntity, Map<String, Object> updates) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JsonNode jsonNode = objectMapper.convertValue(updates, JsonNode.class);

        Iterator<Map.Entry<String, JsonNode>> fields = jsonNode.fields();
        while (fields.hasNext()) {
            Map.Entry<String, JsonNode> entry = fields.next();
            String fieldName = entry.getKey();
            JsonNode value = entry.getValue();

            try {
                // Use reflection or specific setters/getters to update fields in marketEntity
                Field field = marketEntity.getClass().getDeclaredField(fieldName);
                field.setAccessible(true);
                Object parsedValue = objectMapper.treeToValue(value, field.getType());
                field.set(marketEntity, parsedValue);
            } catch (NoSuchFieldException | IllegalAccessException | JsonProcessingException e) {
                // Handle exceptions as needed
                e.printStackTrace();
            }
        }
    }

//    @Override
//    public void bookSale(String name) {
//        BookEntity bookEntity = bookRepo.findByName(name).orElseThrow(NotFoundException::new);
//        //TODO
//    }

}
