package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.domain.BookEntity;
import org.example.domain.MarketEntity;
import org.example.dto.BookDto;
import org.example.dto.BookResponseDto;
import org.example.exception.NotFoundException;
import org.example.repo.BookRepo;
import org.example.repo.MarketRepo;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService{
    private final MarketRepo marketRepo;
    private final BookRepo bookRepo;
    private final ModelMapper modelMapper;

    @Override
    public BookResponseDto createBook(Long id, BookDto bookDto) {
        MarketEntity marketEntity = marketRepo.findById(id).orElseThrow();
        List<BookEntity> allByName = bookRepo.findAllByName(bookDto.getName()).orElseThrow(NotFoundException::new);

        BookEntity bookEntity = modelMapper.map(bookDto, BookEntity.class);
        if(!allByName.isEmpty()){
            bookEntity.setCount(allByName.size()+bookDto.getCount());
        }
        else {
            bookEntity.setCount(bookDto.getCount());
        }
        bookEntity.setMarketEntity(marketEntity);
        marketEntity.getBooks().add(bookEntity);

        if (marketEntity.getBookCount()==null){
            marketEntity.setBookCount(1);
        }else {
            marketEntity.setBookCount(marketEntity.getBookCount() + 1);
        }
        marketEntity.setAmount(marketEntity.getAmount()+bookDto.getPrice());

        marketRepo.save(marketEntity);
        return modelMapper.map(bookEntity,BookResponseDto.class);
    }
    @Override
    public BookResponseDto findBook(Long id) {
        BookEntity bookEntity = bookRepo.findById(id).orElseThrow();
        return modelMapper.map(bookEntity,BookResponseDto.class);
    }

    @Override
    public List<BookResponseDto> findBookByName(String name) {
        List<BookEntity> byName = bookRepo.findAllByName(name).orElseThrow(NotFoundException::new);
        return byName.stream()
                .map(book -> modelMapper.map(book, BookResponseDto.class))
                .collect(Collectors.toList());
    }
}
