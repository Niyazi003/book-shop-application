package org.example.service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
@Service
//@RequiredArgsConstructor
public class OpenAIService {
    @Value("${openai.api.key}")
    private String openaiApiKey ;
    private final String OPENAI_API_URL = "https://api.openai.com/v1/completions";
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    public OpenAIService(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

//    public String getBookSuggestions(String genre) {
//        HttpHeaders headers = new HttpHeaders();
//        headers.set("Authorization", "Bearer " + openaiApiKey);
//        headers.set("Content-Type", "application/json");
//
//        //String body = "{ \"model\": \"text-davinci-003\", \"prompt\": \"Bana " + genre + " türünde önerilen kitapların bir listesini ver.\", \"max_tokens\": 100 }";
//        String body = "{ \"model\": \"gpt-3.5-turbo\", \"messages\": [{\"role\": \"user\", \"content\": \"Bana " + genre + " türünde önerilen kitapların bir listesini ver.\"}], \"max_tokens\": 100 }";
//
//        HttpEntity<String> entity = new HttpEntity<>(body, headers);
//
//        ResponseEntity<String> response = restTemplate.exchange(OPENAI_API_URL, HttpMethod.POST, entity, String.class);
//        String responseBody = response.getBody();
//
//        try {
//            JsonNode jsonNode = objectMapper.readTree(responseBody);
//            return jsonNode.get("choices").get(0).get("text").asText();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "An error occurred while processing the request.";
//        }


}
