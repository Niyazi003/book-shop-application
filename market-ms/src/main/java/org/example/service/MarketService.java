package org.example.service;

import jakarta.persistence.Entity;
import org.example.domain.MarketEntity;
import org.example.dto.MarketDto;
import org.example.dto.MarketResponseDto;
import org.springframework.data.jpa.repository.EntityGraph;

import java.util.Map;

public interface MarketService {
    MarketResponseDto createMarket(MarketDto marketDto);

    MarketResponseDto findMarket(Long id);
    MarketResponseDto updateMarket(Long id, MarketDto marketDto);
     void applyUpdates(MarketEntity marketEntity, Map<String, Object> updates);
}
