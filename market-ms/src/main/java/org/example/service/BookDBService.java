package org.example.service;

import org.example.domain.BookEntity;

import java.util.List;

public interface BookDBService {
    List<BookEntity> callDB();
}
