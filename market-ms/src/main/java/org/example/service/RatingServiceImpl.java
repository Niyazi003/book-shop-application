package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.domain.RatingEntity;
import org.example.repo.RatingRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RatingServiceImpl implements RatingService{
    private final RatingRepo ratingRepo;
    @Override
    public void ratingScore(String prompt) {
        RatingEntity byName = ratingRepo.findByName(prompt);
        List<RatingEntity> all = ratingRepo.findAll();
        if (byName==null){
            ratingRepo.save(RatingEntity
                    .builder()
                    .name(prompt)
                    .count(1)
                    .ratingScore((double) ((1.0/ratingRepo.findSumOfAllCounts())*100))
                    .build());

        }
        else {
            byName.setCount(byName.getCount()+1);
            byName.setRatingScore(((double)(byName.getCount())/ratingRepo.findSumOfAllCounts())*100);
            ratingRepo.save(byName);
        }
        for (RatingEntity i :all){
            i.setRatingScore(((double)i.getCount())/ratingRepo.findSumOfAllCounts()*100);
            ratingRepo.save(i);
        }
    }
}
