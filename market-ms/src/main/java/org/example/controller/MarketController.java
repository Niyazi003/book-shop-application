package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.domain.BookEntity;
import org.example.domain.MarketEntity;
import org.example.domain.RatingEntity;
import org.example.dto.MarketDto;
import org.example.dto.MarketResponseDto;
import org.example.exception.NotFoundException;
import org.example.repo.BookRepo;
import org.example.repo.MarketRepo;
import org.example.repo.RatingRepo;
import org.example.service.MarketService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/market")
@RequiredArgsConstructor
public class MarketController {
   private final MarketRepo marketRepo;
   private final MarketService marketService;
   private final ModelMapper modelMapper;
   private final BookRepo bookRepo;
   private final RatingRepo ratingRepo;

   @Autowired
   RestTemplate restTemplate;
   @PostMapping("/create-market")
   public MarketResponseDto createMarket(@RequestBody MarketDto marketDto){
      return marketService.createMarket(marketDto);
   }

   @GetMapping("/{id}")
   public MarketResponseDto find(@PathVariable Long id){
      return marketService.findMarket(id);
   }
   @PatchMapping("/update/{id}")
   public MarketResponseDto updateMarket(@PathVariable Long id, @RequestBody Map<String, Object> updates) {
      MarketEntity marketEntity = marketRepo.findById(id)
              .orElseThrow(NotFoundException::new);

      marketService.applyUpdates(marketEntity, updates);

      marketRepo.save(marketEntity);
      return modelMapper.map(marketEntity, MarketResponseDto.class);
   }
//   @GetMapping("/books/search")
//   public BookEntity findBookByName(@RequestParam String name) {
//      //TODO
//      List<BookEntity> bookEntities = bookRepo.findAllByName(name).orElseThrow(NotFoundException::new);
//      Long id = bookEntities.getFirst().getId();
//      BookEntity bookEntity = bookRepo.findById(id).orElseThrow();
//      bookRepo.deleteById(id);
//      return bookEntity;
//   }
   @GetMapping("/books/{name}")
   public ResponseEntity<BookEntity> getBookByName(@PathVariable String name){
      BookEntity book = bookRepo.findByName(name).orElseThrow();
      if (book != null) {
      return ResponseEntity.ok(book);
      }
      return ResponseEntity.notFound().build();
   }

   @DeleteMapping("/books/{name}")
   public ResponseEntity<Void> deleteBookByName(@PathVariable String name){
      BookEntity bookEntity = bookRepo.findByName(name).orElseThrow();
      if (bookEntity!=null) {
         return ResponseEntity.ok().build();}

         return ResponseEntity.notFound().build();

   }


   @GetMapping("/rating")
   public List<RatingEntity> getRating(){
      return ratingRepo.findAll();
   }


}
