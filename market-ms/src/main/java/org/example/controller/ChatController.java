package org.example.controller;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.example.domain.RatingEntity;
import org.example.model.ChatGPTRequest;

import org.example.model.ChatGptResponse;
import org.example.repo.RatingRepo;
import org.example.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/chat")
//@RequiredArgsConstructor
public class ChatController {
    private final RatingRepo ratingRepo;
    private final RatingService ratingService;
    @Value("${openai.model}")
    private String model;
    @Value("${openai.api.url}")
    private String apiURL;
//    @Autowired(required = true)
//    private RestTemplate template;

    private final RestTemplate template;
    @Autowired
    public ChatController(RatingRepo ratingRepo, RatingService ratingService, @Qualifier("template") RestTemplate template) {
        this.ratingRepo = ratingRepo;
        this.ratingService = ratingService;
        this.template = template;
    }

    @GetMapping("/bot")
    public String chat(@RequestParam("prompt") String prompt) {
     //   ratingService.ratingScore(prompt);
        ChatGPTRequest request = new ChatGPTRequest(model, "recommend me a few book about " + prompt);
        ChatGptResponse chatGptResponse = template.postForObject(apiURL, request, ChatGptResponse.class);
        return chatGptResponse.getChoices().get(0).getMessage().getContent();

    }
}
