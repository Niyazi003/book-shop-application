package org.example.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.example.dto.BookDto;
import org.example.dto.BookResponseDto;
import org.example.repo.BookRepo;
import org.example.service.BookService;
import org.example.service.OpenAIService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;
    @PostMapping("/create/{id}")
    BookResponseDto createBook(@PathVariable Long id, @RequestBody BookDto bookDto){
        return bookService.createBook(id,bookDto);
    }
    @GetMapping("/get-book/{id}")
    public BookResponseDto find(@PathVariable Long id){
        return bookService.findBook(id);
    }


}
