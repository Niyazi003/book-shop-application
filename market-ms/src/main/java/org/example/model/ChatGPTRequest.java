package org.example.model;

import lombok.Data;
import org.example.model.Message;

import java.util.ArrayList;
import java.util.List;
@Data
public class ChatGPTRequest {
    private String model;
    private List<Message> messages;
    private Integer max_tokens=20;

    public ChatGPTRequest(String model, String prompt) {
        this.model = model;
        this.messages = new ArrayList<>();
        this.messages.add(new Message("user",prompt));
    }

}
