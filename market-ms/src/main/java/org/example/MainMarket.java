package org.example;

import lombok.RequiredArgsConstructor;
import org.example.repo.RatingRepo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
@SpringBootApplication
@ComponentScan(basePackages = {"com.example.market-ms, com.example.common"})
public class MainMarket  {
    public static void main(String[] args) {
        SpringApplication.run(MainMarket.class, args);
    }


}