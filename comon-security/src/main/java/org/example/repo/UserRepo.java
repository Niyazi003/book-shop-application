package org.example.repo;
import org.example.domain.UserEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepo extends JpaRepository<UserEntity,Long> {
    @EntityGraph(attributePaths = "authorities")
    Optional<UserEntity> findByUsername(String name);
}
