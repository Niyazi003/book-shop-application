package org.example.service;

import org.example.dto.SignInDto;
import org.example.dto.SignInResponse;

public interface AuthService {
    SignInResponse signIn(SignInDto signInDto);
}
