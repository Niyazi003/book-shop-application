package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.auth.service.JwtService;
import org.example.dto.JwtAccessToken;
import org.example.dto.RefreshToken;
import org.example.dto.SignInDto;
import org.example.dto.SignInResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService{
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtService jwtService;
    @Override
    public SignInResponse signIn(SignInDto sing) {
        UsernamePasswordAuthenticationToken token=
                new UsernamePasswordAuthenticationToken(
                        sing.getName()
                        ,sing.getPassword());

        final Authentication authenticate = authenticationManagerBuilder.getObject().authenticate(token);
        System.out.println("Authentication is "+ authenticate);
        String jwt = jwtService.issueToken(authenticate);
        return SignInResponse.builder()
                .accessToken(JwtAccessToken.builder()
                        .token(jwt)
                        .build())
                .refreshToken(RefreshToken.builder()
                        .token("refresh")
                        .build())
                .build();

    }

}
