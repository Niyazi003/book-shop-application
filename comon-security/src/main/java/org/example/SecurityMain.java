package org.example;

import lombok.RequiredArgsConstructor;
import org.example.auth.service.JwtService;
import org.example.domain.UserEntity;
import org.example.repo.UserRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@EnableJpaRepositories(basePackages = "com.example.comon-security.repo")
//@EntityScan(basePackages = "com.example.comon-security.domain")
@SpringBootApplication
@RequiredArgsConstructor
public class SecurityMain implements CommandLineRunner {
    private final JwtService jwtService;
    private final UserRepo userRepo;
    public static void main(String[] args) {
        SpringApplication.run(SecurityMain.class,args);
    }

    @Override
    public void run(String... args) throws Exception {

        UserEntity userEntity=UserEntity
                .builder()
                .username("user")
                .password("{noop}1234")
                .accountNonExpired(true)
                .accountNonLocked(true)
                .enabled(true)
                .credentialsNonExpired(true)
                .build();
        //userRepo.save(userEntity);

    }
}