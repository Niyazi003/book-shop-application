package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.config.JwtTokenConfigProperties;
import org.example.dto.SignInDto;
import org.springframework.http.HttpHeaders;

import org.example.dto.SignInResponse;
import org.example.service.AuthService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;

import static org.example.config.JwtAuthRequestFilter.ACCESS_TOKEN_COOKIE;
import static org.example.config.JwtAuthRequestFilter.REFRESH_TOKEN_COOKIE;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final JwtTokenConfigProperties properties;
    private final AuthService authService;

    @PostMapping("/sign-in")
    public ResponseEntity<SignInResponse> signIn(@RequestBody SignInDto signDto) {
        HttpHeaders httpHeaders = new HttpHeaders();
        final SignInResponse signInResponse = authService.signIn(signDto);
        setCookies(httpHeaders, signInResponse);
        return new ResponseEntity<>(signInResponse, httpHeaders, HttpStatus.OK);
    }

    @DeleteMapping("/sign-out")
    public ResponseEntity<Void> signOut() {
        HttpHeaders httpHeaders = new HttpHeaders();
        clearCookies(httpHeaders);
        return new ResponseEntity<>(null, httpHeaders, HttpStatus.NO_CONTENT);
    }

    private void clearCookies(HttpHeaders httpHeaders) {
        ResponseCookie accessToken = ResponseCookie.from(ACCESS_TOKEN_COOKIE,
                        "")
                .httpOnly(true)
                .secure(false)
                .path("/")
                .maxAge(Duration.ZERO)
                .sameSite("LAX")
                .build();
        ResponseCookie refreshToken = ResponseCookie.from(REFRESH_TOKEN_COOKIE, "")
                .httpOnly(true)
                .secure(false)
                .path("/")
                .maxAge(Duration.ZERO)
                .sameSite("LAX")
                .build();
        httpHeaders.add(HttpHeaders.SET_COOKIE, accessToken.toString());
        httpHeaders.add(HttpHeaders.SET_COOKIE, refreshToken.toString());
    }

    private void setCookies(HttpHeaders httpHeaders, SignInResponse signInResponse) {
        ResponseCookie accessToken = ResponseCookie.from(ACCESS_TOKEN_COOKIE,
                        signInResponse.getAccessToken().getToken())
                .httpOnly(true)
                .secure(false)
                .path("/")
                .maxAge(properties.getJwtProperties().getTokenValidityInSeconds())
                .sameSite("LAX")
                .build();
        ResponseCookie refreshToken = ResponseCookie.from(REFRESH_TOKEN_COOKIE,
                        signInResponse.getRefreshToken().getToken())
                .httpOnly(true)
                .secure(false)
                .path("/")
                .maxAge(properties.getJwtProperties().getRefreshTokenValidityInSeconds())
                .sameSite("LAX")
                .build();
        httpHeaders.add(HttpHeaders.SET_COOKIE, accessToken.toString());
        httpHeaders.add(HttpHeaders.SET_COOKIE, refreshToken.toString());
    }

}
