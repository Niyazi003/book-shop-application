package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.auth.service.JwtService;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class SecurityApi {
    private final JwtService jwtService;
    public static final String BEARER="Bearer";
    @GetMapping("/public")
    public String getAll(@RequestHeader String authorization){

        String bearer = authorization.substring(BEARER.length()).trim();
        return "hello "+ jwtService.parseToken(bearer);
    }
    @GetMapping("/authenticated")
    public String getAuthenticated(Authentication authentication){
        return jwtService.issueToken(authentication);
    }

    @GetMapping("/role-user")
    public String getRoleUser(Authentication authentication){
        return "hello "+ authentication.getName();
    }
    @GetMapping("/role-admin")
    public String getRoleAdmin(Authentication authentication){
        return "hello "+ authentication.getName();
    }
   
}
