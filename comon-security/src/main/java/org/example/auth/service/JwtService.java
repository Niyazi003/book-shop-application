package org.example.auth.service;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.micrometer.common.util.StringUtils;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.example.config.JwtTokenConfigProperties;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class JwtService {
    private Key key;
    private final JwtTokenConfigProperties properties;
    @PostConstruct
    public void init(){
        byte[] keyBytes;
        if (StringUtils.isBlank(properties.getJwtProperties().getSecret())) {
            throw new RuntimeException("Token config not found");
        }
        keyBytes = Decoders.BASE64.decode(properties.getJwtProperties().getSecret());
        key = Keys.hmacShaKeyFor(keyBytes);

    }
    public String issueToken(Authentication authentication){
        init();
       return Jwts.builder()
                .setSubject(authentication.getName())
                .claim("authorities",authentication.getAuthorities())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration
                        .ofSeconds(properties.getJwtProperties()
                                .getTokenValidityInSeconds()))))
                        .signWith(key, SignatureAlgorithm.HS256).compact();

    }

    public Claims parseToken(String token){
        init();
        return Jwts.parserBuilder().setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();

    }
}
