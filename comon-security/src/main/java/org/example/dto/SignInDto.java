package org.example.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString(exclude = "password")
public class SignInDto {
    private String name;
    private String password;

}
