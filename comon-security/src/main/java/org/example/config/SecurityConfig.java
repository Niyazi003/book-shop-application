package org.example.config;

import lombok.RequiredArgsConstructor;
import org.springframework.aot.generate.ValueCodeGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
@Configuration
@RequiredArgsConstructor
public class SecurityConfig {
    private final JwtAuthFilterConfigurerAdapter jwtAuthFilterConfigurerAdapter;
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

         http.authorizeHttpRequests(request->request.requestMatchers("/test/public").permitAll()
                         .requestMatchers("/v3/api-docs/**","/swagger-ui/**").permitAll()
                .anyRequest().authenticated())
                .formLogin(Customizer.withDefaults());
         http.apply(jwtAuthFilterConfigurerAdapter);
         return http.build();
    }

}
