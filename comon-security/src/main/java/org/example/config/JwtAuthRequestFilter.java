package org.example.config;

import io.jsonwebtoken.Claims;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.auth.service.JwtService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class JwtAuthRequestFilter extends OncePerRequestFilter {
    public static final String ACCESS_TOKEN_COOKIE = "jwtAccessToken";
    public static final String REFRESH_TOKEN_COOKIE = "refreshToken";
    public static final String AUTHORITIES_CLAIM = "authorities";

    private final JwtService jwtService;


    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws IOException, ServletException {
        final Optional<Authentication> authenticationOptional = getAuthentication(
                extractJwt(getCookie(httpServletRequest)));
        authenticationOptional.ifPresent(authentication
                -> SecurityContextHolder.getContext().setAuthentication(authentication));
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private Optional<Cookie> getCookie(HttpServletRequest httpServletRequest) {
        Cookie[] cookies = Optional.ofNullable(httpServletRequest.getCookies())
                .orElse(new Cookie[0]);
        return Arrays.stream(cookies).filter(cookie -> cookie.getName().equals(ACCESS_TOKEN_COOKIE))
                .findFirst();
    }

    public Optional<String> extractJwt(Optional<Cookie> cookieOptional) {
        if (cookieOptional.isEmpty()) {
            return Optional.empty();
        }

        Cookie cookie = cookieOptional.get();

        final String jwt = cookie.getValue()
                .trim();
        return Optional.of(jwt);
    }
    private Optional<Authentication> getAuthentication(Optional<String> jwt) {
        if (jwt.isEmpty()) {
            return Optional.empty();
        }

        try {
            final Claims claims = jwtService.parseToken(jwt.get());
            final UsernamePasswordAuthenticationToken authenticationToken =
                    new UsernamePasswordAuthenticationToken(claims.getSubject(),
                            "",getUserAuthorities (claims));
            return Optional.of(authenticationToken);
        } catch (RuntimeException exception) {
            log.trace("Jwt parsing failed ", exception);
        }
        System.out.println("Jwt is expired");
        return Optional.empty();
    }
    private Collection<? extends GrantedAuthority> getUserAuthorities(Claims claims) {
        List<?> roles = claims.get(AUTHORITIES_CLAIM, List.class);
        return roles
                .stream()
                .map(a -> new SimpleGrantedAuthority(a.toString()))
                .collect(Collectors.toList());
    }

}
