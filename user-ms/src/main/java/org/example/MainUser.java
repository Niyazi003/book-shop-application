package org.example;

import lombok.RequiredArgsConstructor;
import org.example.domain.UserEntity;
import org.example.repo.UserRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.example.user-ms, com.example.common"})
public class MainUser implements CommandLineRunner {
    public static void main(String[] args) {
       SpringApplication.run(MainUser.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
      UserEntity userEntity= UserEntity
                .builder()
                .username("niyazi")
                .password("12sfd")
                .amount(50.8)
                .build();
        //userRepo.save(userEntity);

    }
}