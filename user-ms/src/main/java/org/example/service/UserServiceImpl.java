package org.example.service;
import lombok.RequiredArgsConstructor;

import org.example.domain.UserBookEntity;
import org.example.dto.BookDto;
import org.example.repo.UserBookRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class UserServiceImpl implements UserService{
   // private final ModelMapper modelMapper;
    private final RestTemplate restTemplate;
    private final UserBookRepo bookRepository;


    private static final String MARKET_MS_URL = "http://localhost:8080/api/market/books/";
    @Autowired
    public UserServiceImpl(@Qualifier("restTemplate") RestTemplate restTemplate, UserBookRepo bookRepository) {
        this.restTemplate = restTemplate;
        this.bookRepository = bookRepository;
    }

    @Override
    public boolean bookSale(String bookName) {
        UserBookEntity book = restTemplate.getForObject(MARKET_MS_URL + bookName, UserBookEntity.class);
        if (book == null) {
            return false;
        }

    bookRepository.save(book);

        restTemplate.delete(MARKET_MS_URL + bookName);

        return true;
    }

}
