package org.example.repo;

import org.example.domain.UserBookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserBookRepo extends JpaRepository<UserBookEntity,Long> {
}
